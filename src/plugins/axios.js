"use strict";

import axios from "axios";

export default {
  install: (app, options) => {
    axios.defaults.headers.get['Accept'] = 'application/vnd.npm.install-v1+json; q=1.0, application/json; q=0.8, */*';


    let config = {
      // baseURL: process.env.baseURL || process.env.apiUrl || ""
      // timeout: 60 * 1000, // Timeout
      // withCredentials: true, // Check cross-site Access-Control
    };

    const _axios = axios.create(config);

    _axios.interceptors.request.use(
        function(config) {
          // Do something before request is sent
          return config;
        },
        function(error) {
          // Do something with request error
          return Promise.reject(error);
        }
    );

// Add a response interceptor
    _axios.interceptors.response.use(
        function(response) {
          // Do something with response data
          return response;
        },
        function(error) {
          // Do something with response error
          return Promise.reject(error);
        })


    app.axios = _axios;
    window.axios = _axios;
    Object.defineProperties(app.config.globalProperties, {
      axios: {
        get() {
          return _axios;
        }
      },
      $axios: {
        get() {
          return _axios;
        }
      },
    });

    app.provide('axios', options)
  }
}
