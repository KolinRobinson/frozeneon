import axios from './plugins/axios'
import { createApp } from 'vue'
import App from './App.vue'
import store from './store'
import PrimeVue from 'primevue/config';
import Paginator from 'primevue/paginator';
import Button from 'primevue/button';

createApp(App).use(store).use(axios).use(PrimeVue).component('Paginator', Paginator).component('Button', Button).mount('#app')
