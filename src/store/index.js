import { createStore } from 'vuex'

export default createStore({
  state: {
    package_items: [],
    page: 0,
    total: 0
  },
  getters: {
    PACKAGES: state => {
      return state.package_items;
    },
    TOTAL: state => {
      return state.total;
    },
  },
  mutations: {
    SAVE_PACKAGES: (state, payload) => {
      state.package_items = []
      payload.objects.forEach(item =>{
        state.package_items.push(item.package)
      })
      state.total = payload.total
    },
    NEW_PAGE: (state, payload) =>{
      state.page = payload
    }
  },
  actions: {
    GET_PACKAGES:  async (context, names) => {
      // eslint-disable-next-line no-undef
      let {data} = await axios.get(`-/v1/search?size=10&from=${context.state.page * 10}&text=${names}`);
      console.log(data)
      context.commit('SAVE_PACKAGES', data);
    },
    GET_PAGE({commit}, index){
      commit('NEW_PAGE', index)
    }
  },
  modules: {
  }
})
